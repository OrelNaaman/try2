import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection = this.db.collection('students');

  public deleteStudent(id:string){
    this.db.doc(`students/${id}`).delete();
  }

  addStudent(math:number, psycho:number, payment:boolean){
    const student = {math:math, psycho:psycho, payment:payment};
    this.studentsCollection.add(student);
  }

  getStudent():Observable<any[]>{
    this.studentsCollection = this.db.collection(`students`);
    return this.studentsCollection.snapshotChanges()
  }

  constructor(private db:AngularFirestore) { }
}
