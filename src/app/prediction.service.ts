
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://n8i7tperjh.execute-api.us-east-1.amazonaws.com/beta2'; 
  
  predict(math:number, psycho:number,payment:boolean):Observable<any>{
    let json = {
      "data": 
        {
          "math": math,
          "psycho": psycho,
          "payment": payment
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}
