import { Student } from './../interfaces/student';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() math:number;
  @Input() psycho:number;
  @Input() payment:boolean;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>()
  @Output() closeEdit = new EventEmitter<null>()
  isErrorMat:boolean = false;
  isErrorpsy:boolean = false;

  tellParentToClose(){
    this.closeEdit.emit();
  }

  updateParent(){
    let student:Student = {id:this.id, math:this.math, psycho:this.psycho, payment:this.payment};
    if(this.psycho < 0 || this.psycho > 800){
      this.isErrorpsy = true;
    }
    else if(this.math < 0 || this.math > 100){
      this.isErrorMat = true;
    }
    else{
    this.update.emit(student);
    if(this.formType == "Add Student"){
      this.math = null;
      this.psycho = null;
      this.payment = false;
    }
   }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
