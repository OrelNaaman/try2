import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students:Student[];
  students$;
  editstate = [];
  addStudentFormOpen = false;
  panelOpenState = false;
  
  add(student:Student){
    this.studentsService.addStudent(student.math, student.psycho, student.payment);
  }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(id);
  }

  constructor(public authService:AuthService, private studentsService:StudentsService) { }

  ngOnInit(): void {
        this.students$ = this.studentsService.getStudent();
        this.students$.subscribe(
          docs =>{
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              if(student.result){
                student.saved = true; 
              }
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
      }
}
