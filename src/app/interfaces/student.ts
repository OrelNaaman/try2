export interface Student {
    id:string,
    math:number,
    psycho:number,
    payment:boolean,
    result?:string,
    saved?:boolean
}
