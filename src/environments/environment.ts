// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig :  {
    apiKey: "AIzaSyD7siKlW5RkizekmvJrEIZB7mlxYLzF75o",
    authDomain: "hello-try2.firebaseapp.com",
    projectId: "hello-try2",
    storageBucket: "hello-try2.appspot.com",
    messagingSenderId: "1038083793495",
    appId: "1:1038083793495:web:c867f5b385e2d8cbee3912"
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
